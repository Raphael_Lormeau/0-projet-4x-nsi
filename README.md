# Projet NSI 4X

## Information est idées :

- **Thèmes** : Fantaisie
- **Ressources** : Bois, Pierre, Métal, Mana, Or, (Nourriture ?)
- **Idées de partie** : Quatre joueurs au quatre coins se battent, un marché au centre, pacifique, permet de faire du commerce à n'importe quel moment. Chaque joueur a une ressources dominantes dont il doit profiter.

## Travail préparatoire :

### Classes :

- `Troupes()` :
    - **Attribut** :
        - Nom : Nom de la troupe.
        - Cout : Prix de la troupe.
        - Attaque : Attaque de la troupe.
        - Defense : Defense de la troupe.
        - Rapidité : Déplacement possible sur la carte.
        - Skills : Compétences de la troupe lui donnant une utilisation spécifique.
        - Portée : Portée de la troupe sur la carte.
    - **Méthode** :
        - action() : Action que la troupe peut faire a chaque tour
        - deplacement() : Déplace l'unité à gauche, droite, en haut ou en bas.
    - **Objets** :
        - Ouvrier : Simple unité pour construire des batiments.
        - Cavalier : Unité rapide mais plutot faible.
        - Fantassin : Simple unité de combat.
        - Archer : Unité faible mais possédant une grande portée.
        - Mage : Unité faible mais possédant une grande portée.
        - Marchand : Unité ayant pour seul but de marchander.
        - Voleur : Unité ayant pour seul but de voler les autres joueurs.

- `Batiments()` : 
    - **Attribut** :
        - Ressources : Cout du batiment.
        - Temps : Nombre de tour pour construire le batiments.
    - **Méthode** :
    - **Objets** :
        - Caserne : Batiment pour former les troupes.

- `Carte()` : Une carte de 20x20.
    - **Attribut** :
    - **Méthode** :
        - charger_carte() : Lance la carte sur l'image
        - generer_carte() : Créer une carte de facon aléatoire.
        - repartition() : ???
    - **Objets** :

- `Case()` : 
    - **Attribut** :
    - **Méthode** :
        - pratiquable() : Regarde si la zone ou l'unité veut aller est pratiquables actuellement.
    - **Objets** :

- `Joueur()` :
    - **Attribut** :
        - Limites ressources : Les limites de ressources pouvant etre stockées.
        - Ressources actuelles : Les ressources que l'on a actuellement.
        - joueurs : Joueur 1, 2, 3 et 4.
    - **Méthode** :
    - **Objets** :

- `Jeu()` : 
    - **Attribut** :
        - Nb_tours : Le nombre de tour de jeu déjà fait.
    - **Méthode** :
        - tour_de_jeu() : Effctue un tour de jeu (en appuyant sur un bouton ?) 
        - construction_bat() : Diminue le tour de constructon de batiments de 1 et si le nombre de tour restants est 0, construit le batiment.
    - **Objets** :

- `Recherche()` : Arbre de recherches (A voir plus tard).

- `Ressources()` : Sert à définir les ressources de tous les joueurs :
    - **Attribut** :
        - Name:  de la ressource
        - RessInitiales : Nombre de ressources en début de partie
        - RessMin: Ressources minimales possible
        - RessMax: Ressources maximales atteignables en partie
        - RessAct: Ressources actuelles des joueurs
    - **Méthode** :
        - reduire_ressAct : réduit les ressources actuelles ( en cas d'achats, de constructions etc)
        - augmenter_ressAct : augmente les ressources actuelles quand elles sont récupérées par un joueur
        - augmenter_ressMax : augmente la capacité de stockage des ressources
        - reduire_ressMax : réduit la capacité de stockage des ressources
    - **Objets** :
        - Bois : une des quatres ressources principales du jeu servant aux constructions etc
        - Pierre : une des quatres ressources principales du jeu servant aux constructions etc
        - Métal : une des quatres ressources principales du jeu servant aux constructions etc
        - Mana : une des quatres ressources principales du jeu servant aux constructions etc
        - Or : Ressource pouvant être utilisée pour acheter des items au marché
