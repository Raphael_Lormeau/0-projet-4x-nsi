﻿import json

def charger_dico_tech() :
    fichier = open(file="datas/dico_tech.json", mode="r")
    dico_tech = json.load(fichier)
    fichier.close()
    return dico_tech


class Technologie():
    def __init__(self, num_tech):
        dico_tech = charger_dico_tech()
        self.tech = dico_tech[str(num_tech)]
        self.num_tech = num_tech
        self.nom = self.tech["nom"]
        self.cout = self.tech["cout"]
        self.bonus = self.tech["bonus"]
        self.prerequis = self.tech["prerequis"]
        self.decouvert = False

        self.cout_actuel = self.cout

    def decouvrir(self):
        if self.cout_actuel == 0:
            self.decouvert = True

    def est_decouvert(self):
        return self.decouvert

    def rechercher(self, point_recherche:int):
        self.cout_actuel -= point_recherche
        self.decouvrir()

    def __str__(self):
        txt = self.nom
        txt += " ({0} / {1}) ".format(self.cout_actuel, self.cout)
        return txt

if __name__ == "__main__":
    t = Technologie(1)
    print(t)
    t.rechercher(5)
    print(t)
    t.rechercher(5)
    print(t)
    if t.est_decouvert():
        print(t.nom + " est découverte !")