from Case import *
from random import choice
import json

def charger_carte(map_name:str):
    fichier = open(file="maps/"+map_name+".json", mode="r")
    mappe = json.load(fichier)
    fichier.close()
    return mappe


class Carte():

    def __init__(self, hauteur:int=-1, largeur:int=-1, map_name:str=""):
        self.grille = []
        self.dico_terrain = charger_info_terrain()
        if hauteur>0 and largeur>0:
            self.nb_lignes = hauteur
            self.nb_colonnes = largeur
            self.generer_carte(self.nb_lignes, self.nb_colonnes)
        elif map_name != "":
            self.generer_carte_prefaite(charger_carte(map_name))
        else:
            raise AttributeError("Les dimensions doivent être positives ou le jeu de fichier non vide.")


    def __str__(self):
        txt = ""
        for ligne in self.grille:
            for case in ligne:
                txt += case.name + "-"
            txt += "\n"
        return txt

    def generer_carte(self, nb_lignes: int, nb_colonnes: int):
        les_terrains = list(self.dico_terrain.keys())
        for y in range(nb_lignes):
            Ligne = []
            for x in range(nb_colonnes):
                if x == 0 and y == 0:
                    terrain = "capitale"
                elif x == 0 and y == self.nb_lignes-1:
                    terrain = "capitale"
                elif x == self.nb_colonnes-1 and y == 0:
                    terrain = "capitale"
                elif x == self.nb_colonnes-1 and y == self.nb_lignes-1:
                    terrain = "capitale"
                else:
                    terrain = choice(les_terrains)
                    while terrain == "capitale":
                        terrain = choice(les_terrains)
                Ligne.append(Case(terrain, x, y))
            self.grille.append(Ligne)

    def generer_carte_prefaite(self, mappe):
        for y in range(len(mappe)):
            LigneGrille = []
            for x in range(len(mappe[y])):
                LigneGrille.append(Case(mappe[x][y], x, y))
            self.grille.append(LigneGrille)



    ### GESTION DES UNITES ###
    def deplacer_unite(self, i,j,direction:str):
        """direction peut valoir "HAUT", "BAS", "GAUCHE", "DROITE". """
        if direction == "HAUT":
            self.grille[i][j-1].unit = self.grille[i][j].unit
            self.grille[i][j].unit = None
        if direction == "BAS":
            self.grille[i][j+1].unit = self.grille[i][j].unit
            self.grille[i][j].unit = None
        if direction == "GAUCHE":
            self.grille[i-1][j].unit = self.grille[i][j].unit
            self.grille[i][j].unit = None
        if direction == "DROITE":
            self.grille[i+1][j].unit = self.grille[i][j].unit
            self.grille[i][j].unit = None

    def ajouter_unite(self, unit, ligne, colonne):
        self.grille[ligne][colonne].ajouter_unite(unit)

    def retirer_unite(self, ligne, colonne):
        self.grille[ligne][colonne].retirer_unite()

    ### GESTION DES BÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡TIMENTS ###
    def ajouter_batiment(self, bat, ligne, colonne):
        self.grille[ligne][colonne].ajouter_batiment(bat)

    def retirer_batiment(self, ligne, colonne):
        self.grille[ligne][colonne].retirer_batiment()


if __name__ == "__main__":
    c1 = Carte(8, 6)
    print(c1)

    c2 = Carte(map_name = "carte1")
    print(c2)