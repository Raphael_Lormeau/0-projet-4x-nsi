class Ville_Marche():
    def __init__(self, emplacement_marche):
        self.emplacement_marche = emplacement_marche
        ### A SIMPLIFIER, JE PENSE ###
        self.prix_bois_a = 150
        self.prix_pierre_a = 150
        self.prix_fer_a = 150
        self.prix_mana_a = 150
        self.prix_bois_v = 100
        self.prix_pierre_v = 100
        self.prix_fer_v = 100
        self.prix_mana_v = 100
        self.qte_bois_a = 0
        self.qte_pierre_a = 0
        self.qte_fer_a = 0
        self.qte_mana_a = 0
        self.qte_bois_v = 0
        self.qte_pierre_v = 0
        self.qte_fer_v = 0
        self.qte_mana_v = 0

    def qte_vendue(self):
        return [self.qte_bois_v, self.qte_fer_v, self.qte_pierre_v, self.qte_mana_v]

    def qte_achete(self):
        return [self.qte_bois_a, self.qte_fer_a, self.qte_pierre_a, self.qte_mana_a]

    def variation_prix(self):
        if self.qte_bois_a >= 500:
            self.prix_bois_a += 25
        if self.qte_pierre_a >= 500:
            self.prix_pierre_a += 25
        if self.qte_fer_a >= 500:
            self.prix_fer_a += 25
        if self.qte_mana_a >= 500:
            self.prix_mana_a += 25
        if self.qte_bois_v >= 500:
            self.prix_bois_v -= 25
        if self.qte_pierre_v >= 500:
            self.prix_pierre_v -= 25
        if self.qte_fer_v >= 500:
            self.prix_fer_v -= 25
        if self.qte_mana_v >= 500:
            self.prix_mana_v -= 25


    def inflation(self):
        self.prix_bois_a   = int(self.prix_bois_a*1.05)       ### Il y a 5% d'inflation
        self.prix_pierre_a = int(self.prix_pierre_a*1.05)
        self.prix_fer_a    = int(self.prix_fer_a*1.05)
        self.prix_mana_a   = int(self.prix_mana_a*1.05)


    def prix_achat(self):
        return [self.prix_bois_a, self.prix_fer_a, self.prix_pierre_a, self.prix_mana_a]


    def prix_vente(self):
        return [self.prix_bois_v, self.prix_fer_v, self.prix_pierre_v, self.prix_mana_v]

    def acheter_bois(self, qte_bois)->int:
        #modifier self.qte_bois_a
        self.qte_bois_a += qte_bois
        #modifier self.prix_bois_a
        self.variation_prix()
        #renvoyer la quantité de bois
        return self.qte_bois_a

    def vendre_bois(self, qte_bois)->int:
        #modifier self.qte_bois_v
        self.qte_bois_v += qte_bois
        #modifier self.prix_bois_v
        self.variation_prix()
        #renvoyer qte_bois * self.prix_bois_a
        return self.qte_bois_v

    def acheter_pierre(self, qte_pierre)->int:
        #modifier self.qte_bois_a
        self.qte_pierre_a += qte_pierre
        #modifier self.prix_bois_a
        self.variation_prix()
        #renvoyer la quantité de bois
        return self.qte_pierre_a

    def vendre_pierre(self, qte_pierre)->int:
        #modifier self.qte_bois_v
        self.qte_pierre_v += qte_pierre
        #modifier self.prix_bois_v
        self.variation_prix()
        #renvoyer qte_bois * self.prix_bois_a
        return self.qte_pierre_v

    def acheter_fer(self, qte_fer)->int:
        #modifier self.qte_bois_a
        self.qte_fer_a += qte_fer
        #modifier self.prix_bois_a
        self.variation_prix()
        #renvoyer la quantitÃ© de bois
        return self.qte_fer_a

    def vendre_fer(self, qte_fer)->int:
        #modifier self.qte_bois_v
        self.qte_fer_v += qte_fer
        #modifier self.prix_bois_v
        self.variation_prix()
        #renvoyer qte_bois * self.prix_bois_a
        return self.qte_fer_v

    def acheter_mana(self, qte_mana)->int:
        #modifier self.qte_bois_a
        self.qte_mana_a += qte_mana
        #modifier self.prix_bois_a
        self.variation_prix()
        #renvoyer la quantité de bois
        return self.qte_mana_a

    def vendre_mana(self, qte_mana)->int:
        #modifier self.qte_bois_v
        self.qte_mana_v += qte_mana
        #modifier self.prix_bois_v
        self.variation_prix()
        #renvoyer qte_bois * self.prix_bois_a
        return self.qte_mana_v

if __name__ == "__main__":
    m = Ville_Marche((6,5))
    print("Prix du bois : ", m.prix_bois_a)
    m.vendre_bois(5)
    print("Prix du bois : ", m.prix_bois_a)
    m.acheter_bois(800)
    print("Prix du bois : ", m.prix_bois_a)
    m.inflation()
    print("Prix du bois : ", m.prix_bois_a)