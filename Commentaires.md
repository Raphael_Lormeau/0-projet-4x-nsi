# Commentaires

## 2022-12-03

- un travail qui avance à grands pas
- voir remarques et conseils dans les tickets
- des fonctions ont été ajoutées au code
- des interfaces fouillées

## 2022-10-04

- Le fichier README est très précis, même si certains éléments auraient été intéressants dans les tickets.
- 8 tickets ouverts, mais seulement 2 en cours
- Sur les 4 devOps, seuls 2 ont posté du code
- Le code est encore très insuffisant : 
    - La classe `Batiments` ne contient vraiment que 2 lignes de code utiles
    - La classe `Troupes` contient un constructeur. 
- Des remarques dans les tickets. 
