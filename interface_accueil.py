﻿from tkinter import *
#from tkinter.ttk import *
from interface_action import *
from interface_info_joueur import *
from interface_carte import *
from interface_guide import *
from interface_ressources import *
from interface_btn_importants import *
from Carte import *



class MenuDemarrage() :
    def __init__(self, fenetre, jeu):
        """ Lance la frame de début de jeu (l'accueil) """
        self.fen = fenetre
        self.img_fermer = PhotoImage(file ="img/icon_fermer.png")
        self.jeu = jeu
        #self.img_fond = PhotoImage(file = 'img/ex_fond')


        self.frame_debut = Frame(master = self.fen)#, background = self.img_fond)
        self.lab_debut = Label(master=self.frame_debut, text= "Bienvenue sur ce 4X !!!")
        self.btn_debut = Button(master=self.frame_debut, text = "Commencer", command= lambda:self.demarrer())
        self.btn_fermer_debut = Button(master=self.frame_debut, text= "Quitter", image = self.img_fermer, compound = TOP, command = self.fen.destroy)
        self.btn_guide = Button(master=self.frame_debut, text = "Guide", command= lambda:self.guide())
        self.can_gauche = Canvas(master=self.frame_debut, width = 400, height = 400)
        self.can_droite = Canvas(master=self.frame_debut, width = 400, height = 400)
        self.can_gauche.grid(row=1, column=1, rowspan=6)
        self.can_droite.grid(row=1, column=3, rowspan=6)
        self.btn_debut.grid(row=3, column=2)
        self.lab_debut.grid(row=2, column=2)
        self.btn_guide.grid(row=5, column=2)
        self.btn_fermer_debut.grid(row=8, column=2)

        self.frame_carte = Interface_carte(self, self.jeu)
        self.frame_action = Interface_action(self.fen, self, self.jeu)

    """ Fonction permettant de lancer le jeu a partir de l'acceuil"""
    def demarrer(self) :
        # Frame contenant les actions restantes et possibles à faire pendant le tour.
        self.frame_debut.destroy()
        # Frame contenant la carte du jeu.
        self.frame_carte.frame.grid(row = 1, column = 0, rowspan=5)

        self.frame_action.frame.grid(row=2, column=3, rowspan=3)
        # Onglet contenant les infos sur le joueur (Ressources et armée).
        self.onglet_info_joueur = Interface_info_joueur(self.fen, self.jeu)

        self.frame_ressources = Interface_Ressources(self.fen, self.jeu.joueur1, self.jeu)
        self.frame_ressources.frame.grid(row=0, column=0)

        self.frame_btn_important = Interface_btn_importants(self.fen, self.jeu)
        self.frame_btn_important.frame.grid(row=5, column=3, sticky = S)


    """ Fonction qui permet de lancer le guide. """
    def guide(self) :
        self.fen_guide = Interface_guide(self.fen)


if __name__ == "__main__":
    fenetre = Tk()
    jeu = Jeu(50)
    menu = MenuDemarrage(fenetre, jeu)
    menu.frame_debut.grid(row=1, column=2)
    fenetre.mainloop()