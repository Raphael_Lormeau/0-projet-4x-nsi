﻿from tkinter import *
from tkinter.ttk import *
from jeu import *

class Interface_action() :
    def __init__(self, fenetre, nom, jeu) :
        self.fen = fenetre # La fenetre du jeu,
        self.nom = nom # le jeu  de l'interface,
        self.jeu = jeu # Le jeu du jeu,
        self.frame = Frame(master=self.fen) # La frame qui contient tout pour l'action,
        self.carte = jeu.carte # la carte du jeu,
        self.joueur_act = jeu.joueur_actuel # Le joueur qui joue actuellement,
        self.case_entite = None # La case sélectionné avec une action a faire,
        self.action = None # Action que le joueur va choisir,
        self.entite = None # Entite que le joueur a choisi.
        self.dico_troupe = charger_dico_troupe()

        """ Lance la frame regroupant toutes les actions. """
        self.frame_act = Frame(master = self.frame, borderwidth = 10 , relief = "solid")
        self.frame_act.grid(row=2, column=3, rowspan=3)

        """ Lance l'onglet montrant les actions restantes a faire ce tour. """
        self.boite_onglet_act_restante = Notebook(master = self.frame_act)#, height = 200, width = 400)
        self.boite_onglet_act_restante.grid(row=1, column=1)

        """ Onglet affichant les troupes. """
        self.onglet_act_restante_troupe = Frame(master = self.boite_onglet_act_restante)
        self.boite_onglet_act_restante.add(self.onglet_act_restante_troupe, text = "Troupes :")
        self.label_expli_act_res_t = Label(master = self.onglet_act_restante_troupe, text = "Troupe disponible :")
        self.label_expli_act_res_t.grid(row=1, column=1)
        self.frame_troupe_dispo = Frame(master = self.onglet_act_restante_troupe)
        self.frame_troupe_dispo.grid(row=2, column=1)

        """ Onglet affichant les batiments. """
        self.onglet_act_restante_bat = Frame(master = self.boite_onglet_act_restante)
        self.boite_onglet_act_restante.add(self.onglet_act_restante_bat, text = "Batiments :")
        self.label_expli_act_res_b = Label(master = self.onglet_act_restante_bat, text = "Batiment disponible :")
        self.label_expli_act_res_b.grid(row=1, column=1)
        self.frame_batiment_dispo = Frame(master = self.onglet_act_restante_bat)
        self.frame_batiment_dispo.grid(row=2, column=1)

        """ Lance la frame montrant les actions possibles pour cette troupes/batiment. """
        self.frame_possibilite = Frame(master = self.frame_act)

        """ Lance la frame montrant les options pour l'action choisi. """
        self.frame_option = Frame(master = self.frame_act)

        self.btn_confirmer = Button(master=self.frame_option, text = "Confirmer", command=lambda:self.confirmer(depart, arrive, action, entite))


    def action_case(self) :
        """ Fonction qui affiche les actions disponibles a faire sur la case selectionnée,
        elle doit se lancer à chaque fois que la case selectionnée change. """
        self.btn_confirmer.destroy()
        self.btn_confirmer = Button(master=self.frame_option, text = "Confirmer", command=lambda:self.confirmer(depart, arrive, action, entite))

        if self.case_entite == None or self.action == None:
            self.frame_troupe_dispo.destroy()
            self.frame_batiment_dispo.destroy()
            self.frame_troupe_dispo = Frame(master = self.onglet_act_restante_troupe)
            self.frame_batiment_dispo = Frame(master = self.onglet_act_restante_bat)
            self.frame_troupe_dispo.grid(row=2, column=1)
            self.frame_batiment_dispo.grid(row=2, column=1)

            self.frame_possibilite.destroy()
            self.frame_possibilite = Frame(master = self.frame_act)
            self.frame_possibilite.grid(row=2, column=1, columnspan=2)

            self.frame_option.destroy()
            self.frame_option = Frame(master = self.frame_act)
            self.frame_option.grid(row=3, column=1)

            if self.jeu.case_actuelle == None :
                self.label_troupe_dispo = Label(master=self.frame_troupe_dispo, text = "Pas de case !")
                self.label_batiment_dispo = Label(master=self.frame_batiment_dispo, text = "Pas de case !")
                self.label_troupe_dispo.grid(row=1, column=1)
                self.label_batiment_dispo.grid(row=1, column=1)
            else :
                self.label_case = Label(master=self.frame_act, text= "Case sélectionnée : " + str(self.jeu.case_actuelle))
                self.label_case.grid(row=5, column=1)
                self.colonne = self.jeu.case_actuelle[0]
                self.ligne = self.jeu.case_actuelle[1]
                self.case_entite = self.jeu.case_actuelle
                if self.carte.grille[self.colonne][self.ligne].building == None or self.carte.grille[self.colonne][self.ligne].building.joueur != "joueur" + str(self.jeu.joueur_actuel.num_joueur) :
                    self.label_batiment_dispo = Label(master=self.frame_batiment_dispo, text = "Pas de batiment !")
                    self.label_batiment_dispo.grid(row=1, column=1)
                else :
                    batiment = self.carte.grille[self.colonne][self.ligne].building
                    self.btn_batiment = Button(master = self.frame_batiment_dispo, text = batiment.name, command=lambda:self.affiche_possible(batiment))
                    self.btn_batiment.grid(row=1, column=1)

                if self.carte.grille[self.colonne][self.ligne].unit == None or self.carte.grille[self.colonne][self.ligne].unit.joueur != "joueur" + str(self.jeu.joueur_actuel.num_joueur) :
                        self.label_troupe_dispo = Label(master=self.frame_troupe_dispo, text = "Pas de troupe !")
                        self.label_troupe_dispo.grid(row=1, column=1)
                elif self.carte.grille[self.colonne][self.ligne].unit.move <= 0 :
                    self.label_troupe_dispo = Label(master=self.frame_troupe_dispo, text = "Pas de troupe !")
                    self.label_troupe_dispo.grid(row=1, column=1)
                else :
                    troupe = self.carte.grille[self.colonne][self.ligne].unit
                    self.btn_troupe = Button(master = self.frame_troupe_dispo, text = troupe.nom, command=lambda:self.affiche_possible(troupe))
                    self.btn_troupe.grid(row=1, column=1)

        elif self.action == "Construire" or self.action == "Endommager" or self.action == "Voler" :
            self.affiche_resultat(self.case_entite, None, self.action, self.entite)

        else :
            self.affiche_resultat(self.case_entite, self.jeu.case_actuelle, self.action, self.entite)


    def annuler(self) :
        self.frame_option.destroy()
        self.frame_option = Frame(master = self.frame_act)
        self.frame_option.grid(row=3, column=1)
        self.frame_possibilite.destroy()
        self.frame_possibilite = Frame(master=self.frame_act)
        self.frame_possibilite.grid(row=2, column=1, columnspan=2)
        self.action = None


    def affiche_possible(self, entite) :
        """ Fonction qui affiche les actions en fonction de l'unité ou batiments choisi. """
        self.frame_possibilite.destroy()
        if str(type(entite)) == "<class 'Batiments.Batiments'>" : # Rajouter une section formation dans les batiments afin de faire comme pour les troupes.
            if entite.name == "caserne" :
                self.frame_possibilite = Frame(master=self.frame_act)
                self.lab_expli_possible = Label(master=self.frame_possibilite, text = "Différentes possibilités :")
                self.btn_act_former = Button(master=self.frame_possibilite, text = "Former", command = lambda:self.choisir_emplacement("Former", entite))
                self.btn_act_ameliorer = Button(master=self.frame_possibilite, text = "Améliorer", command = lambda:self.choisir_emplacement("Ameliorer", entite))

                self.frame_possibilite.grid(row=2, column=1, columnspan=2)
                self.lab_expli_possible.grid(row=1, column=1, columnspan=2)
                self.btn_act_former.grid(row=2, column=1)
                self.btn_act_ameliorer.grid(row=2, column=2)

            elif entite.name == "capitale" :
                self.frame_possibilite = Frame(master=self.frame_act)
                self.lab_expli_possible = Label(master=self.frame_possibilite, text = "Différentes possibilités :")
                self.btn_act_former = Button(master=self.frame_possibilite, text = "Former", command = lambda:self.choisir_emplacement("Former", entite))

                self.frame_possibilite.grid(row=2, column=1, columnspan=2)
                self.lab_expli_possible.grid(row=1, column=1, columnspan=2)
                self.btn_act_former.grid(row=2, column=1)
        else :
            self.frame_possibilite = Frame(master = self.frame_act)
            self.frame_possibilite.grid(row=2, column=1)
            self.lab_expli_possible = Label(master=self.frame_possibilite, text = "Différentes possibilités :")

            i = 0
            r = 2
            for action in self.dico_troupe[entite.nom]["skill"] :
                if action == "deplacer" :
                    self.btn_act_deplacer = Button(master=self.frame_possibilite, text = "Déplacer", command = lambda:self.choisir_emplacement("Deplacer", entite))
                    self.btn_act_deplacer.grid(row=r, column=i)
                elif action == "attaquer" :
                    self.btn_act_attaque = Button(master=self.frame_possibilite, text = "Attaquer", command = lambda:self.choisir_emplacement("Attaquer", entite))
                    self.btn_act_attaque.grid(row=r, column=i)
                elif action == "construire" :
                     self.btn_act_constru = Button(master=self.frame_possibilite, text = "Construire", command = lambda:self.choisir_emplacement("Construire", entite))
                     self.btn_act_constru.grid(row=r, column=i)
                elif action == "endommager" :
                    self.btn_act_endommager = Button(master=self.frame_possibilite, text= "Endommager", command = lambda:self.choisir_emplacement("Endommager", entite))
                    self.btn_act_endommager.grid(row=r, column=i)
                elif action == "saboter" :
                    self.btn_act_saboter = Button(master=self.frame_possibilite, text= "Saboter", command = lambda:self.choisir_emplacement("Saboter", entite))
                    self.btn_act_saboter.grid(row=r, column=i)
                elif action == "voler" :
                    self.btn_act_voler = Button(master=self.frame_possibilite, text= "Voler", command = lambda:self.choisir_emplacement("Voler", entite))
                    self.btn_act_voler.grid(row=r, column=i)
                elif action == "echange" :
                    self.btn_act_echange = Button(master=self.frame_possibilite, text= "Echange", command = lambda:self.choisir_emplacement("Echange", entite))
                    self.btn_act_echange.grid(row=r, column=i)
                i = i + 1
                if i > 5 :
                    r = r + 1
                    i = 0
                # A continuer car il manque des cas.
            self.lab_expli_possible.grid(row=1, column=0, columnspan=i)

        self.btn_annuler = Button(master=self.frame_act, text="Annuler", command=lambda:self.annuler())
        self.btn_annuler.grid(row=6, column=1)


    def action_valide(self, action, entite) :
        """ Fonction qui a pour but de tester si l'action qui a été sélectionnée est bien réalisable. """
        move_horizontale = self.case_entite[0] - self.jeu.case_actuelle[0]
        move_verticale = self.case_entite[1] - self.jeu.case_actuelle[1]
        x_arrive = self.jeu.case_actuelle[0]
        y_arrive = self.jeu.case_actuelle[1]
        if action == "Deplacer" and self.carte.grille[x_arrive][y_arrive].unit == None:
            if entite == "cavalier" :
                if move_horizontale == -2 or move_horizontale == 2 :
                    return move_verticale == 0
                elif move_verticale == -2 or move_verticale == 2 :
                    return move_horizontale == 0
                elif move_horizontale == -1 or move_horizontale == 1 :
                    return move_verticale == 0 or move_verticale == -1 or move_verticale == 1
                elif move_verticale == -1 or move_verticale == 1 :
                    return move_horizontale == 0 or move_horizontale == -1 or move_horizontale == 1
                else :
                    return False
            else :
                if move_horizontale == -1 or move_horizontale == 1 :
                    return move_verticale == 0
                elif move_verticale == -1 or move_verticale == 1 :
                    return move_horizontale == 0
                else :
                    return False
        elif action == "Attaquer" and self.carte.grille[x_arrive][y_arrive].unit != None and self.carte.grille[x_arrive][y_arrive].unit.joueur != "joueur" + str(self.jeu.joueur_actuel.num_joueur):
            if entite == "archer" :
                if move_horizontale == -2 or move_horizontale == 2 :
                    return move_verticale == 0
                elif move_verticale == -2 or move_verticale == 2 :
                    return move_horizontale == 0
                elif move_horizontale == -1 or move_horizontale == 1 :
                    return move_verticale == 0 or move_verticale == -1 or move_verticale == 1
                elif move_verticale == -1 or move_verticale == 1 :
                    return move_horizontale == 0 or move_horizontale == -1 or move_horizontale == 1
                else :
                    return False
            else :
                if move_horizontale == -1 or move_horizontale == 1 :
                    return move_verticale == 0
                elif move_verticale == -1 or move_verticale == 1 :
                    return move_horizontale == 0
                else :
                    return False
        elif action == "Saboter" and self.carte.grille[x_arrive][y_arrive].unit != None and self.carte.grille[x_arrive][y_arrive].unit.joueur != "joueur" + str(self.jeu.joueur_actuel.num_joueur):
            if move_horizontale == -1 or move_horizontale == 1 :
                return move_verticale == 0
            elif move_verticale == -1 or move_verticale == 1 :
                return move_horizontale == 0
            else :
                return False
        elif action == "Echanger" and (self.carte.grille[x_arrive][y_arrive].building.name == "Marche" or self.carte.grille[x_arrive][y_arrive].unit.nom == "Marchand") :
            if move_horizontale == -1 or move_horizontale == 1 :
                return move_verticale == 0
            elif move_verticale == -1 or move_verticale == 1 :
                return move_horizontale == 0
            else :
                return False
        """ else :
            return True"""

    def affiche_resultat(self, depart, arrive, action, entite) :
        """ Fonction qui affiche le résultat de l'action. """
        if arrive == None :
            pass
        elif self.action_valide(action, entite) :
            self.lab_resultat.config(text =entite.nom + " va " + action + " de " + str(depart) + " a " + str(arrive))
            """ Bouton qui permettra de lancer la fonction faisant l'action. """
            self.btn_confirmer.destroy()
            self.btn_confirmer = Button(master=self.frame_option, text = "Confirmer", command=lambda:self.confirmer(depart, arrive, action, entite))
            self.btn_confirmer.grid(row=5, column=1)

        else :
            self.lab_resultat.config(text = "Cette action n'est pas réalisable !")

    def confirmer(self, depart, arrive, action, entite) :
        if action == "Deplacer" :
            self.jeu.deplacer_unite(entite, depart, arrive)
        elif action == "Attaquer" :
            cible = self.carte.grille[arrive[0]][arrive[1]].unit
            self.carte.grille[depart[0]][depart[1]].unit.attaquer(cible)
            if cible.vie <= 0 :
                self.jeu.retirer_unite(cible)
        elif action == "Saboter" :
            cible = self.carte.grille[arrive[0]][arrive[1]].unit
            self.carte.grille[depart[0]][depart[1]].unit.saboter(cible)
        elif action == "Echanger" :
            self.carte.grille[depart[0]][depart[1]].unit.echanger()
        elif action == "Voler" :
            self.carte.grille[depart[0]][depart[1]].unit.voler()
        else :
            pass
        self.annuler()
        self.action_case()


    def choisir_emplacement(self, action, entite) :
        """ Fonction qui permet de sélectionner la case ou l'on veut faire l'action. """
        self.action = action
        self.entite = entite
        self.frame_option.destroy()
        if action == "Deplacer" :
            self.frame_option = Frame(master = self.frame_act)
            self.lab_expli_option = Label(master=self.frame_option, text = "Choisissez l'endroit puis confirmer.")
            self.lab_resultat = Label(master=self.frame_option)
            self.frame_option.grid(row=3, column=1)
            self.lab_expli_option.grid(row=1, column=1)
            self.lab_resultat.grid(row=4, column=1, columnspan=3)

        if action == "Attaquer" :
            self.frame_option = Frame(master = self.frame_act)
            self.lab_expli_option = Label(master=self.frame_option, text = "Choisissez l'endroit puis confirmer.")
            self.lab_resultat = Label(master=self.frame_option)
            self.frame_option.grid(row=3, column=1)
            self.lab_expli_option.grid(row=1, column=1)
            self.lab_resultat.grid(row=4, column=1)

        elif action == "Saboter" :
            self.frame_option = Frame(master = self.frame_act)
            self.lab_expli_option = Label(master=self.frame_option, text = "Choisissez l'endroit puis confirmer.")
            self.lab_resultat = Label(master=self.frame_option)
            self.frame_option.grid(row=3, column=1)
            self.lab_expli_option.grid(row=1, column=1)
            self.lab_resultat.grid(row=4, column=1, columnspan=3)

        elif action == "Construire" :
            self.frame_option = Frame(master = self.frame_act)
            self.lab_expli_option = Label(master=self.frame_option, text = "Construire a la case " + str(self.case_entite) + " ?")
            self.lab_resultat = Label(master=self.frame_option)
            self.frame_option.grid(row=3, column=1)
            self.lab_resultat.grid(row=4, column=2, columnspan=3)
            self.lab_expli_option.grid(row=1, column=2)

            self.btn_caserne = Button(master=self.frame_option, text = "Caserne", command = lambda:self.jeu.ajouter_batiment(self.jeu.num_joueur+1, "caserne", self.case_entite))
            self.btn_ecurie = Button(master=self.frame_option, text = "Ecurie", command = lambda:self.jeu.ajouter_batiment(self.jeu.num_joueur+1, "ecurie", self.case_entite))
            self.btn_scierie = Button(master=self.frame_option, text = "Scierie", command = lambda:self.jeu.ajouter_batiment(self.jeu.num_joueur+1, "scierie", self.case_entite))
            self.btn_caserne.grid(row=2, column=1)
            self.btn_ecurie.grid(row=2, column=2)
            self.btn_scierie.grid(row=2, column=3)

        elif action == "Echanger" :
            self.frame_option = Frame(master = self.frame_act)
            self.lab_expli_option = Label(master=self.frame_option, text = "Choisissez l'endroit puis confirmer.")
            self.lab_resultat = Label(master=self.frame_option)
            self.lab_resultat.grid(row=4, column=1, columnspan=3)
            self.frame_option.grid(row=3, column=1)

        elif action == "Voler" :
            self.frame_option = Frame(master = self.frame_act)
            self.lab_resultat = Label(master=self.frame_option)
            self.lab_resultat.config(text = "Voler la case " + str(self.case_entite) + " ?")
            self.lab_resultat.grid(row=4, column=1, columnspan=3)
            self.frame_option.grid(row=3, column=1)

        elif action == "Endommager" :
            self.frame_option = Frame(master = self.frame_act)
            self.lab_resultat = Label(master=self.frame_option)
            self.lab_resultat.config(text = "Endommager la case " + str(self.case_entite) + " ?")
            if self.action_valide("endommager",self.entite) :
                self.btn_confirmer_endommager = Button(master=self.frame_option, text="Confirmer", command=lambda:self.jeu.endommager_bat(self.carte.grille[self.case_entite[0]][self.case_entite[1]].building, self.entite))
                self.btn_confirmer_endommager.grid(row=2, column=1, columnspan=3)
            else :
                self.lab_resultat.config(text = "Cette action n'est pas réalisable !")
            self.lab_resultat.grid(row=4, column=1, columnspan=3)
            self.frame_option.grid(row=3, column=1)

        elif action == "Former" :
            self.frame_option = Frame(master = self.frame_act)
            self.lab_resultat = Label(master=self.frame_option)
            self.lab_expli_option = Label(master=self.frame_option, text = "Quelle troupe doit etre formée ?")

            self.frame_option.grid(row=3, column=1)
            self.lab_resultat.grid(row=4, column=1, columnspan=3)
            self.lab_expli_option.grid(row=1, column=2)

            if self.entite.can_build_foot_unit == True :
                self.btn_former_ouvrier = Button(master=self.frame_option, text = "Ouvrier", command = lambda:self.jeu.ajouter_unite(self.jeu.num_joueur+1, "ouvrier", self.case_entite))
                self.btn_former_fantassin = Button(master=self.frame_option, text = "Fantassin", command = lambda:self.jeu.ajouter_unite(self.jeu.num_joueur+1, "fantassin", self.case_entite))
                self.btn_former_archer = Button(master=self.frame_option, text = "Archer", command = lambda:self.jeu.ajouter_unite(self.jeu.num_joueur+1, "archer", self.case_entite))

                self.btn_former_ouvrier.grid(row=2, column=1)
                self.btn_former_fantassin.grid(row=2, column=2)
                self.btn_former_archer.grid(row=2, column=3)

            if self.entite.can_build_horse_unit == True :
                self.btn_former_cavalier = Button(master=self.frame_option, text = "Cavalier", command = lambda:self.jeu.ajouter_unite(self.jeu.num_joueur+1, "cavalier", self.case_entite))
                self.btn_former_cavalier.grid(row=2, column=4)

        elif action == "Ameliorer" :
            self.frame_option = Frame(master = self.frame_act)
            self.btn_op_ameliorer = Button(master=self.frame_option, text = "Ameliorer le batiment", command = lambda:print("Ameliorer"))
            self.frame_option.grid(row=3, column=1)
            self.btn_op_ameliorer.grid(row=2, column=2)


if __name__ == "__main__":
    jeu = Jeu(50)
    fenetre = Tk()
    interf = Interface_action(fenetre, "partie test", jeu)
    interf.frame.grid(row=0, column=0)
    fenetre.mainloop()