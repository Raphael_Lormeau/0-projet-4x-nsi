﻿from tkinter import *
from tkinter.ttk import *
from interface_accueil import *
from jeu import *

class Interface() :
    def __init__(self) :
        self.fenetre = Tk()

        self.fenetre.title("Imperial Warrior 0.0.1 ou Planorga, pour les intimes ou Pigasus3000, the king of the pigs")
        ##self.fenetre.attributes("-fullscreen", True)# pour mettre en plein écran.
        self.fenetre.iconbitmap(bitmap = "./img/img.ico")
        self.img_ferme = PhotoImage(file ="img/icon_fermer.png")
        self.btn_fermer = Button(master = self.fenetre, image = self.img_ferme, command = self.fenetre.destroy)

        self.jeu = Jeu(200, self)

        """ Classe permettant d'afficher le menu de démarrage """
        self.menu = MenuDemarrage(self.fenetre, self.jeu)


        self.organiser()

        #self.fenetre.mainloop()

    def organiser(self):
        self.menu.frame_debut.grid(row=1, column=2)

if __name__ == "__main__" :
    interface = Interface()
    interface.fenetre.mainloop()
