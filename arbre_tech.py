﻿from technologie import *

class Arbre_technologique() :
    def __init__(self, tech) :
        self.tech = tech # Technologie du noeud de l'arbre,
        self.gauche = None # Sous-arbre gauche,
        self.droit = None # Sous-arbre droit,
        self.dico_tech = charger_dico_tech() # Dictionnaire contenant les différentes technologies.

    def est_vide(self)->bool : # Fonction qui dit si l'arbre est vide ou non.
        return self.tech == None

    def est_feuille(self)->bool : # Fonction qui dit si le l'arbre est une feuille ou non.
        return self.gauche == None and self.droit == None

    def hauteur(self) :
        if self.est_feuille() :
            return 1
        elif self.droit == None :
            return 1 + self.gauche.hauteur()
        elif self.gauche == None :
            return 1 + self.gauche.hauteur()
        else :
            return 1 + max(self.droit.hauteur(), self.gauche.hauteur())

    def ajouter(self, tech) : # Fonction qui ajoute une technologie à l'arbre.
        assert tech.num_tech != self.tech.num_tech, "Plusieurs fois la meme technologie"
        if tech.num_tech > self.tech.num_tech and self.droit == None :
            self.droit = Arbre_technologique(tech)
        elif tech.num_tech < self.tech.num_tech and self.gauche == None :
            self.gauche = Arbre_technologique(tech)
        else :
            if tech.num_tech > self.tech.num_tech :
                return self.droit.ajouter(tech)
            elif tech.num_tech < self.tech.num_tech :
                return self.gauche.ajouter(tech)

    def treeToList(self) : # Fonction qui transforme un arbre en liste.
        if self.est_feuille() :
            return [(self.tech.nom, self.tech.num_tech)]
        elif self.gauche == None :
            return [(self.tech.nom, self.tech.num_tech)] + self.droit.treeToList()
        elif self.droit == None :
            return [(self.tech.nom, self.tech.num_tech)] + self.gauche.treeToList()
        else :
            return [(self.tech.nom, self.tech.num_tech)] + self.gauche.treeToList() + self.droit.treeToList()

def listToTree(liste:list, arbre:Arbre_technologique, c:int=0) : # Fonction qui transforme une liste en arbre.
    if liste == [] :
        return arbre
    else : # Cette partie ne marche pas.
        arbre = Arbre_technologique(Technologie(int(liste[0][1])))
        liste.pop(0)
        liste_gauche = [liste[0]] + liste[2:4] + liste[2**c-2:]
        liste_droit = [liste[1]] + liste[4:6] + liste[2**c-2:]
        listToTree(liste_gauche, arbre.gauche, c+1)
        listToTree(liste_droit, arbre.droit, c+1)




