﻿from tkinter import *
from tkinter.ttk import *

class Interface_technologie() :
    def __init__(self, fenetre, jeu) :
        """ Lance la frame qui montre l'arbre technologique. """
        self.jeu = jeu
        self.fen = fenetre
        self.page_arbre = Toplevel(master=self.fen)
        self.frame = Frame(master=self.page_arbre)
        self.img_ferme = PhotoImage(file ="img/icon_fermer.png")
        self.btn_ferme = Button(master=self.frame, image=self.img_ferme, command=self.page_arbre.destroy)
        self.titre = Label(master=self.page_arbre, text="Arbre technologique", font="Arial 24")
        self.titre.grid(row=0, column=0)
        self.frame.grid(row=1, column=0)
        self.btn_ferme.grid(row=2, column=0)
        self.hauteur = 800
        self.longueur = 2400

        self.canvas_arbre = Canvas(master=self.frame, bg="grey", width=1200, height=400, scrollregion=(0,0,self.longueur,self.hauteur))
        self.canvas_arbre.grid(row=0, column=0)

        self.scroll_bar_horizontale = Scrollbar(self.frame,
                                    orient = HORIZONTAL,
                                    command = self.canvas_arbre.xview)
        self.scroll_bar_horizontale.grid(row = 1, column = 0, sticky = EW)
        self.canvas_arbre.config(xscrollcommand = self.scroll_bar_horizontale.set)

        self.scroll_bar_verticale = Scrollbar(self.frame,
                                    orient = VERTICAL,
                                    command = self.canvas_arbre.yview)
        self.scroll_bar_verticale.grid(row = 0, column = 1, sticky = NS)
        self.canvas_arbre.config(yscrollcommand = self.scroll_bar_verticale.set)

        self.dessiner_arbre(self.jeu.joueur_actuel.arbre_tech, 100, 400)


    def creer_bouton(self, arbre, longueur, hauteur) :
        self.btn_tech = Button(master=self.frame, text=arbre.tech.nom, command=lambda:print(arbre.tech))
        self.btn_tech_w = self.canvas_arbre.create_window(longueur,hauteur, window=self.btn_tech)

    def dessiner_arbre(self, arbre, longueur, hauteur) :
        if arbre == None :
            pass
        elif arbre.est_feuille() :
            self.creer_bouton(arbre, longueur, hauteur)
        else :
            self.creer_bouton(arbre, longueur, hauteur)
            self.dessiner_arbre(arbre.gauche, longueur+100, hauteur+50)
            self.dessiner_arbre(arbre.droit, longueur+100, hauteur-50)




