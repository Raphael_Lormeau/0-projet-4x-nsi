class Ressources(): #La classe gère les ressources en général donc les valeurs de tous les joueurs, leur max et min
    def __init__(self, name) :
        self.nom  = name # Nom de la ressource
        self.mini = 0 # Valeur minimale de ressources pouvant être obtenues
        self.maxi = 999 # Valeur maximale de ressources pouvant être obtenues
        self.act  = 0 #Valeur actuelle du nombre de ressources possédées par le joueur

    def __str__(self):
        return self.nom + ":" + str(self.act) + "/" + str(self.maxi)
    def reduire(self, valeur:int):
        self.act -= valeur #>=RessMin

    def augmenter(self, valeur:int):
        self.act += valeur #<= RessMax

    def augmenter_max(self, valeur:int):
        self.max += valeur

    def reduire_max(self, valeur:int):
        self.max -= valeur

    def quantite(self):
        return self.act

if __name__ == '__main__':
    r = Ressources("Bois")
    r.augmenter(15)
    print(r)
    r.reduire(35)
    print(r)
    r.augmenter(99999999)
    print(r)