﻿from tkinter import *
from tkinter.ttk import *
from interface_accueil import *
import json

class Interface_guide() :
    def __init__(self, fenetre) :

        with open(file="datas/dico_guide.json", mode="r") as fichier:
            TEXTE = json.load(fichier)
        """ Lance la frame qui montre le guide du jeu. """
        self.fen = fenetre
        self.guide = Toplevel(self.fen)
        self.img_ferme = PhotoImage(file = "./img/icon_question.png")
        self.frame_guide = Frame(master=self.guide)
        self.titre_guide = Label(master=self.frame_guide, text="Guide du jeu", font="Arial 24")
        self.btn_retour_menu = Button(master=self.frame_guide, text="Fermer guide", command=self.guide.destroy)
        self.frame_guide.grid(row=2, column=1)
        self.titre_guide.grid(row=1, column=1)
        self.btn_retour_menu.grid(row=4, column=1)

        self.onglet_guide = Notebook(master=self.frame_guide, height= 200, width=600)
        self.onglet_guide.grid(row=2, column=1)

        self.frame_ressources = Frame(master=self.onglet_guide)
        self.lab_titre1 = Label(master=self.frame_ressources, text="Ressources :", image=self.img_ferme, compound= LEFT, font = "Arial 16")
        self.lab_resources = Label(master=self.frame_ressources, text=TEXTE["TXT_RESOURCES"])
        self.frame_ressources.grid(row=2, column=1, columnspan=2)
        self.lab_titre1.grid(row=1, column=1, columnspan=2, sticky=EW)
        self.lab_resources.grid(row=2, column=1, columnspan=2)


        self.frame_cases = Frame(master=self.onglet_guide)
        self.lab_titre2 = Label(master=self.frame_cases, text="Cases :", image=self.img_ferme, compound= LEFT, font = "Arial 16")
        self.lab_terrain = Label(master=self.frame_cases, text =TEXTE["TXT_TERRAINS"])
        self.frame_cases.grid(row=2, column=1, columnspan=2)
        self.lab_titre2.grid(row=1, column=1, columnspan=2, sticky=EW)
        self.lab_terrain.grid(row=2, column=1, columnspan=2)


        self.frame_troupes = Frame(master=self.onglet_guide)
        self.lab_titre3 = Label(master=self.frame_troupes, text="Guide des troupes :", image=self.img_ferme, compound= LEFT, font = "Arial 16")
        self.lab_units = Label(master=self.frame_troupes, text=TEXTE["TXT_UNITS"])
        self.frame_troupes.grid(row=2, column=1, columnspan=2)
        self.lab_titre3.grid(row=1, column=1, columnspan=2, sticky=EW)
        self.lab_units.grid(row=2, column=1, columnspan=2)



        self.frame_action = Frame(master=self.onglet_guide)
        self.lab_titre4 = Label(master=self.frame_action, text="Guide des actions :", image=self.img_ferme, compound= LEFT, font = "Arial 16")
        self.lab_actions = Label(master=self.frame_action, text=TEXTE["TXT_ACTIONS"])
        self.frame_action.grid(row=2, column=1, columnspan=2)
        self.lab_titre4.grid(row=1, column=1, columnspan=2, sticky=EW)
        self.lab_actions.grid(row=2, column=1, columnspan=2)


        self.onglet_guide.add(self.frame_ressources, text="Ressources")
        self.onglet_guide.add(self.frame_cases, text="Cases")
        self.onglet_guide.add(self.frame_troupes, text="Troupes")
        self.onglet_guide.add(self.frame_action, text="Actions")

if __name__ == "__main__":
    fenetre = Tk()
    interf = Interface_guide(fenetre)
    fenetre.mainloop()