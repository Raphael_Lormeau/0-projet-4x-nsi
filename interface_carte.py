﻿from tkinter import *
from Carte import *
from jeu import *

TAILLE_TUILES = 64


class Interface_carte():
    def __init__(self, boss, jeu):
        global TAILLE_TUILES
        self.fenetre = boss.fen
        self.boss = boss
        self.jeu = jeu
        #self.nom = nom
        self.frame = Frame(master = self.fenetre)
        self.carte = jeu.carte.grille
        self.hauteur = jeu.carte.nb_lignes
        self.l = jeu.carte.nb_colonnes
        self.mappe = Canvas(master = self.frame,
                            bg="green",
                            height=10*TAILLE_TUILES, width=20*TAILLE_TUILES,
                            scrollregion=(0,0,self.hauteur*TAILLE_TUILES,self.l*TAILLE_TUILES))
        self.mappe.grid(row = 1, column = 0, rowspan=5)

        self.scroll_bar = Scrollbar(self.frame,
                                    orient = VERTICAL,
                                    command = self.mappe.yview)
        self.scroll_bar.grid(row = 1, column = 1, rowspan=5, sticky = NS)

        self.mappe.config(yscrollcommand = self.scroll_bar.set)
        self.mappe.bind("<Button-1>", self.on_button1_click)

        self.charger_images()
        self.charger_carte()

    def on_button1_click(self, event):
        """Récupérer les coordonnées du clic et convertir en coordonnées de grille"""
        self.colonne = int(self.mappe.canvasx(event.x)//64)
        self.ligne = int(self.mappe.canvasy(event.y)//64)
        self.coord_click = (self.colonne, self.ligne)
        self.jeu.case_actuelle = self.coord_click
        self.boss.frame_action.action_case()
        print(self.coord_click)

    def charger_images(self):
        self.images = dict()
        self.images["montagne"] = PhotoImage(file="img/unit_montagne.png")
        self.images["foret"] = PhotoImage(file="img/terrain_foret.png")
        self.images["gisement mana"] = PhotoImage(file="img/terrain_mana.png")
        self.images["gisement fer"] = PhotoImage(file="img/terrain_fer.png")
        self.images["colline"] = PhotoImage(file="img/terrain_colline.png")
        self.images["plaine"] = PhotoImage(file="img/terrain_plaine.png")
        self.images["volcan"] = PhotoImage(file="img/terrain_volcan.png")
        self.images["capitale"] = PhotoImage(file="img/terrain_capitale.png")

        self.images["fantassin"] = PhotoImage(file="img/unit_guerrier.png")
        self.images["ouvrier"] = PhotoImage(file="img/unit_guerrier.png") # Pour les tests, a changer.

        self.images["caserne"] = PhotoImage(file="img/Caserne.png")
        self.images["capitale"] = PhotoImage(file="img/Capitale.png")

    def dessiner_case(self, case:Case):
        global TAILLE_TUILES
        self.mappe.create_image(
            case.x * TAILLE_TUILES,
            case.y * TAILLE_TUILES,
            image = self.images[case.name],
            anchor=NW)

    def charger_carte(self):
        global TAILLE_TUILES
        for i in range(self.l) :
            for j in range(self.hauteur) :
                self.dessiner_case(self.carte[i][j])
                if self.carte[i][j].building != None:
                    self.charger_batt(self.carte[i][j].building)
                if self.carte[i][j].unit != None:
                    self.charger_unite(self.carte[i][j].unit)


    def charger_unite(self, unite:Troupes):
        global TAILLE_TUILES
        unite.identifiant = self.mappe.create_image(
            unite.colonne * TAILLE_TUILES,
            unite.ligne * TAILLE_TUILES,
            image = self.images[unite.nom],
            anchor=NW)

    def charger_batt(self, batt:Batiments):
        global TAILLE_TUILES
        batt.identifiant = self.mappe.create_image(
            batt.hauteur * TAILLE_TUILES,
            batt.largeur * TAILLE_TUILES,
            image = self.images[batt.name],
            anchor=NW)

    def effacer_unite(self, unite:Troupes):
        self.mappe.delete(unite.identifiant)

    def deplacer_unite(self, unite:Troupes, NbCaseDroite, NbCaseBas):
        self.mappe.move(unite.identifiant, NbCaseDroite*TAILLE_TUILES, NbCaseBas*TAILLE_TUILES)
        self.positionner_avant(unite)

    def positionner_avant(self, unite:Troupes):
        self.mappe.tag_raise(unite.identifiant)

    def effacer_batt(self, batt:Batiments):
        self.mappe.delete(batt.identifiant)

    def positionner_avant_batt(self, batt:Batiments):
        self.mappe.tag_raise(batt.identifiant)

if __name__ == "__main__":
    class test:
        def __init__(self):
            self.fen = Tk()
    f = test()
    jeu = Jeu(50, f)
    exemple_carte = charger_carte("carte1")
    i = Interface_carte(f, jeu)
    i.frame.grid()
    f.fen.mainloop()