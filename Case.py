﻿import json

def charger_info_terrain():
    fichier = open(file="datas/dico_terrain.json", mode="r")
    dico_terrain = json.load(fichier)
    fichier.close()
    return dico_terrain

class Case():
    def __init__(self, nom:str, x:int, y:int):
        dico_terrain = charger_info_terrain()

        self.name = nom
        self.praticable = dico_terrain[nom]["praticable"]

        self.building  = None
        self.unit      = None
        self.x, self.y = x, y

    def __str__(self):
        txt = self.name + ":\n"
        txt += "\tBatiment : "+ str(self.building) +"\n"
        txt += "\tUnite    : "+ str(self.unit)
        return txt

    def __repr__(self):
        return str(self)

    def est_praticable(self):
        return self.praticable

    ### GESTION DES COORDONNEES ###
    def coordonnees(self)->tuple:
        return (self.x, self.y)

    def set_coordonnees(self, ligne, colonne):
        self.x = colonne
        self.y = ligne

    def est_adjacente_a(self, other)->bool:
        """Renvoie True ou False selon que les cases
        self et other sont adjacentes ou non (côte à côte)"""
        if (self.x, self.y) == (other.x-1, other.y):
            return True
        elif (self.x, self.y) == (other.x+1, other.y):
            return True
        elif (self.x, self.y) == (other.x, other.y-1):
            return True
        elif (self.x, self.y) == (other.x, other.y+1):
            return True
        else:
            return False

    ### GESTION DES BATIMENTS ###

    def ajouter_batiment(self, bat):
        if self.building == None:
            self.building = bat

    def retirer_batiment(self):
        if self.building != None:
            self.building = None

    def contient_batiment(self)->bool:
        if self.building != None:
            return True
        return False

    ### GESTION DES UNITES ###

    def ajouter_unite(self, unit):
        if self.unit == None:
            self.unit = unit

    def retirer_unite(self):
        if self.unit != None:
            self.unit = None

    def contient_unite(self)->bool:
        if self.unit != None:
            return True
        return False

if __name__ == "__main__":
    from Batiments import *
    from Troupes import *

    u = Troupes("Gégé", "fantassin", 0,0)
    b = Batiments("caserne", "joueur 1")
    c = Case("montagne", 0, 0)
    c.ajouter_batiment(b)
    c.ajouter_unite(u)
    print(c)