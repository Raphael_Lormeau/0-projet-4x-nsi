﻿import json

def charger_info_batiments():
    fichier = open(file="datas/dico_bat.json", mode="r")
    dico_bat = json.load(fichier)
    fichier.close()
    return dico_bat

class Batiments():
    def __init__(self, nom:str, joueur:str, hauteur:int, largeur:int):
        dico_bat = charger_info_batiments()

        self.name = nom
        self.joueur = joueur
        self.level = 1

        self.prod_wood = dico_bat[self.name]["niveau" + str(self.level)]["production"]["bois"]
        self.prod_stone = dico_bat[self.name]["niveau" + str(self.level)]["production"]["pierre"]
        self.prod_steel = dico_bat[self.name]["niveau" + str(self.level)]["production"]["metal"]
        self.prod_mana = dico_bat[self.name]["niveau" + str(self.level)]["production"]["mana"]

        self.cost_wood = dico_bat[self.name]["niveau" + str(self.level)]["cout"]["bois"]
        self.cost_stone = dico_bat[self.name]["niveau" + str(self.level)]["cout"]["pierre"]
        self.cost_steel = dico_bat[self.name]["niveau" + str(self.level)]["cout"]["metal"]
        self.cost_mana = dico_bat[self.name]["niveau" + str(self.level)]["cout"]["mana"]
        self.cost_time = dico_bat[self.name]["niveau" + str(self.level)]["cout"]["temps"]

        self.build_time = self.cost_time

        self.can_build_foot_unit = dico_bat[self.name]["niveau" + str(self.level)]["production"]["infanterie"]
        self.can_build_horse_unit = dico_bat[self.name]["niveau" + str(self.level)]["production"]["cavalerie"]
        self.can_build_boat_unit = dico_bat[self.name]["niveau" + str(self.level)]["production"]["bateau"]

        self.damaged = False

        self.hauteur = hauteur
        self.largeur = largeur

    def __str__(self)->str:
        texte = ""
        if self.build_time != self.cost_time and self.build_time != 0:
            texte += "Temps restant : "+ str(self.build_time)
            return texte
        else:
            texte += self.name
            texte += "\nNiveau : " + str(self.level)
            texte += "\nCe batiment produit " + str(self.prod_wood) + " bois, " + str(self.prod_stone) + " pierre, " + str(self.prod_steel) + " metal, " + str(self.prod_mana) + " mana, " + "par tour."
            return texte

    def __repr__(self):
        return str(self)

    def recolter(self)->tuple:
        """fonction qui renvoie le nombre de ressource que le batiment produit"""
        if self.build_time > 0:
            return 0, 0, 0, 0
        elif self.damaged:
            return self.prod_wood//2, self.prod_steel//2, self.prod_stone//2, self.prod_mana//2
        else:
            return (self.prod_wood, self.prod_steel, self.prod_stone, self.prod_mana)

    def endommager(self)->int:
        """fonction qui endommage le bâtiment"""
        self.damaged = True

    def reparer(self)->int:
        """fonction qui renvoie la production du batiment habituel si il a ete endommager au par avant"""
        self.damaged = False

    def tour_construction(self)->int:
        """fonction qui réduit le temps de construction du batiment de 1"""

        if self.build_time > 0:
            self.build_time -= 1
        return self.build_time

    def ameliorer(self)->None:
        """fonction qui augmente le batiment de 1 niveau"""

        self.level += 1
        self.actualiser()

    def actualiser(self)->None:
        """fonction qui actualise les caracteristiques des batiments"""
        dico_bat = charger_info_batiments()

        self.prod_wood = dico_bat[self.name]["niveau" + str(self.level)]["production"]["bois"]
        self.prod_stone = dico_bat[self.name]["niveau" + str(self.level)]["production"]["pierre"]
        self.prod_steel = dico_bat[self.name]["niveau" + str(self.level)]["production"]["metal"]
        self.prod_mana = dico_bat[self.name]["niveau" + str(self.level)]["production"]["mana"]

        self.cost_wood = dico_bat[self.name]["niveau" + str(self.level)]["cout"]["bois"]
        self.cost_stone = dico_bat[self.name]["niveau" + str(self.level)]["cout"]["pierre"]
        self.cost_steel = dico_bat[self.name]["niveau" + str(self.level)]["cout"]["metal"]
        self.cost_mana = dico_bat[self.name]["niveau" + str(self.level)]["cout"]["mana"]
        self.cost_time = dico_bat[self.name]["niveau" + str(self.level)]["cout"]["temps"]

        self.build_time = self.cost_time

        self.can_build_foot_unit = dico_bat[self.name]["niveau" + str(self.level)]["production"]["infanterie"]
        self.can_build_horse_unit = dico_bat[self.name]["niveau" + str(self.level)]["production"]["cavalerie"]
        self.can_build_boat_unit = dico_bat[self.name]["niveau" + str(self.level)]["production"]["bateau"]


if __name__ == "__main__" :
    caserne = Batiments("caserne", "joueur1")
    scierie = Batiments("scierie", "joueur1")
    capitale = Batiments("capitale", "joueur1")
    print(scierie)
    print(capitale)