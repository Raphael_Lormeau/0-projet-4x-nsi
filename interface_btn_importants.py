﻿from tkinter import *
#from tkinter.ttk import *
from jeu import *
from interface_technologie import *

class Interface_btn_importants():
    def __init__(self, fenetre, jeu):
        self.fenetre = fenetre
        self.frame = Frame(master=self.fenetre)
        self.jeu = jeu
        self.img_fermer = PhotoImage(file ="img/icon_fermer.png")
        self.btn_fermer = Button(master = self.frame,
                                 image = self.img_fermer,
                                 command = self.fenetre.destroy)
        self.btn_nv_tour = Button(master = self.frame,
                                  text = 'Nouveau tour',
                                  command = lambda :self.tour_suivant())
        self.arbre = Button(master = self.frame,
                            text = 'Arbre Technologique',
                            command = lambda:self.arbre_technologique())
        self.label_info = Label(master=self.frame,
                                text = "Tour : " + str(self.jeu.nb_tour) + ", Joueur " + str(self.jeu.joueur_actuel.num_joueur))
        self.organiser()


    def organiser(self):
        self.btn_nv_tour.grid(row = 1, column = 0, sticky = EW)
        self.arbre.grid(row = 1, column = 1, sticky = EW)
        self.btn_fermer.grid(row = 1, column = 2, sticky = EW)
        self.label_info.grid(row=0, column=0)

    def tour_suivant(self) :
        self.jeu.suivant()
        self.label_info.config(text = "Tour : " + str(self.jeu.nb_tour) + ", Joueur " + str(self.jeu.joueur_actuel.num_joueur))

    def arbre_technologique(self) :
        self.frame_arbre_technologique = Interface_technologie(self.fenetre, self.jeu)


if __name__ == "__main__":
    fenetre = Tk()
    fenetre.title = ("4X")
    jeu = Jeu(50)
    barre = Interface_btn_importants(fenetre, jeu)
    barre.frame.grid(row=0, column=0)
    fenetre.mainloop()