﻿from ressources import *
from troupes import *
from Batiments import *
from arbre_tech import *

class Joueur():
    def __init__(self, num_joueur:int):
        self.num_joueur = num_joueur
        self.bois = Ressources("Bois")
        self.pierre = Ressources('Pierre')
        self.fer = Ressources('Fer')
        self.mana = Ressources('Mana')
        self.gold = Ressources('Or')
        self.liste_troupes = []
        self.liste_batiments = []
        self.arbre_tech = Arbre_technologique(Technologie(1)) # Arbre de technologie du joueur (à compléter).
        #self.troupe_selectionnee  = self.liste_troupes[0]
        #self.batiment_selectionne = self.liste_batiments[0]

        self.choisir_boost()

    def choisir_boost(self):
        if self.num_joueur == 1:
            self.boost = "BONUS_bois"
        elif self.num_joueur == 2:
            self.boost = "BONUS_pierre"
        elif self.num_joueur == 3:
            self.boost = "BONUS_fer"
        elif self.num_joueur == 4:
            self.boost = "BONUS_mana"

    def est_mort(self)->bool:
        if self.nb_troupes_actuelles() <= 0 and self.nb_batiments_actuels() <= 0:
            return True
        else:
            return False

    ### Gestion DES UNITES ###
    def ajouter_unite(self, unit:Troupes):
        self.liste_troupes.append(unit)

    def retirer_unite(self, unit:Troupes):
        self.liste_troupes.remove(unit)

    """def unite_suivante(self):
        #changer l'unité sélectionnée sans sortir de la liste        innutile pour nous
        pass"""

    def troupes_actuelles(self):
        return self.liste_troupes


    def nb_troupes_actuelles(self):
        return len(self.liste_troupes)

    ### Gestion DES BATIMENTS ###
    def ajouter_batiment(self, unit:Troupes):
        self.liste_batiments.append(unit)

    def retirer_batiment(self, unit:Batiments):
        self.liste_batiments.remove(unit)

    def diminuer_temps_construction(self):
        for batiment in self.liste_batiments:
            batiment.tour_construction()

    def batiments_actuels(self):
        return self.liste_batiments

    def nb_batiments_actuels(self):
        return len(self.liste_batiments)

    ### Gestion DES RESSOURCES ###
    def recolter_ressources(self):
        for batiment in self.liste_batiments:
            x = batiment.recolter()
            # tenir compte de self.boost
            # x = x * self.boost    # à modifier vu que self.boost est une chaine de carateres
            self.augmenter_ressources(x)

    def ressources_actuelles(self):
        return self.bois.quantite(), self.fer.quantite(), self.pierre.quantite(), self.mana.quantite(), self.gold.quantite()

    def augmenter_ressources(self,qte:tuple):
        self.bois.augmenter(qte[0])
        self.pierre.augmenter(qte[1])
        self.fer.augmenter(qte[2])
        self.mana.augmenter(qte[3])

    def diminuer_ressources(self,qte:tuple):
        self.bois.reduire(qte[0])
        self.pierre.reduire(qte[1])
        self.fer.reduire(qte[2])
        self.mana.reduire(qte[3])


if __name__ == "__main__":
    j = Joueur(1)
    print(j.ressources_actuelles())
    j.augmenter_ressources((0,0,5,10))
    print(j.ressources_actuelles())
    j.diminuer_ressources((0,0,1,8))
    print(j.ressources_actuelles())