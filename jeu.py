﻿from joueur import *
from troupes import *
from Batiments import *
from marche import *
from ressources import *
from Carte import *


class Jeu() :
    def __init__(self, tour_max:int, fenetre) :
        self.nb_tour = 0 # Nombre de tour passé dans le jeu (initialisé à 0).
        self.nb_tour_max = tour_max # Nombre de tour avant de déclarer le gagnant.
        self.fenetre = fenetre # La fenetre du jeu,
        self.joueur1 = Joueur(1) # Initialise le joueur 1,
        self.joueur2 = Joueur(2) # Initialise le joueur 2,
        self.joueur3 = Joueur(3) # Initialise le joueur 3,
        self.joueur4 = Joueur(4) # Initialise le joueur 4,
        self.joueurs = [self.joueur1, self.joueur2, self.joueur3, self.joueur4] # Liste des joueurs,
        self.num_joueur = 0 # Indique le numero du joueur qui joue,
        self.joueur_actuel = self.joueurs[self.num_joueur] # Initialise le joueur actuel avec le joueur 1,
        self.carte = Carte(20,20) # La carte du jeu qui est générée aléatoirement dans Carte,
        self.case_actuelle = None # La case ou le joueur a appuyé,
        self.marche = Ville_Marche((10,10)) # Le marche qui permet d'echanger,
        self.dico_troupe = charger_dico_troupe() # Dico des troupes,
        self.dico_bat = charger_info_batiments() # Dico des batiments,
        self.debut_jeu() # Fonction qui lance le jeu.

    def debut_jeu(self) :
        for joueur in self.joueurs :
            nom_joueur = "joueur" + str(joueur.num_joueur)
            if joueur.num_joueur == 1 :
                base_colonne, base_ligne = 0, 0
            elif joueur.num_joueur == 2 :
                base_colonne, base_ligne = 19, 0
            elif joueur.num_joueur == 3 :
                base_colonne, base_ligne = 19, 19
            else :
                base_colonne, base_ligne = 0, 19
            ouvrier = Troupes(nom_joueur, "ouvrier", base_colonne, base_ligne)
            self.carte.ajouter_unite(ouvrier, base_colonne, base_ligne)
            joueur.liste_troupes.append(ouvrier)

            capitale = Batiments("capitale", nom_joueur, base_colonne, base_ligne)
            self.carte.ajouter_batiment(capitale, base_colonne, base_ligne)
            joueur.liste_batiments.append(capitale)
            joueur.augmenter_ressources((50,50,10,10))
            joueur.gold.augmenter(50)
        ### Test ###
        fantassin = Troupes("joueur1", "fantassin", 18, 0)
        self.carte.ajouter_unite(fantassin, 18, 0)
        joueur.liste_troupes.append(fantassin)
        ############


    def fin_de_tour(self) : # Fonction assurant un tour de jeu.
        self.nb_tour += 1
        if self.nb_tour%10 == 0 and self.nb_tour >= 20 :
            self.marche.inflation()
        for joueur in self.joueurs :
            joueur.recolter_ressources()
            for bat in joueur.liste_batiments :
                if bat.build_time > 0 :
                    bat.build_time = bat.build_time - 1

    def fin_du_jeu(self) : # Fonction vérifiant si le jeu se finit ou pas.
        gagnant = self.gagnant()
        if self.nb_tour == self.nb_tour_max or gagnant != None :
            return True, gagnant
        else :
            return False, None

    def suivant(self) : # Fonction qui permet de passer au prochain joueur.
        self.num_joueur = (self.num_joueur + 1) % len(self.joueurs)
        if self.num_joueur == 0 :
            self.fin_de_tour()
        self.joueur_actuel = self.joueurs[self.num_joueur]
        self.recuperation_unite()
        self.fenetre.menu.frame_ressources.actualiser()

    def meurt(self) :
        if joueur1.est_mort() and joueur1 in joueurs :
            joueurs.remove(joueur1)
        if joueur2.est_mort() and joueur2 in joueurs :
            joueurs.remove(joueur2)
        if joueur3.est_mort() and joueur3 in joueurs :
            joueurs.remove(joueur3)
        if joueur4.est_mort() and joueur4 in joueurs :
            joueurs.remove(joueur4)

    """
    def mode_de_jeu(self) :
         # Fonction permettant de choisir un mode de jeu,
           a faire plus tard quand le jeu fonctionnera.
    """

    def gagnant(self) : # Fonction définissant le gagnant (en fonction du mode choisi).
        if len(self.joueurs) == 1 :
            return self.joueurs[0].num_joueur

    def recuperation_unite(self) : # Fonction qui soigne et remet les points de mouvements des troupes du joueur.
        for i in range(len(self.joueur_actuel.liste_troupes)) :
            self.joueur_actuel.liste_troupes[i].soin()
            self.joueur_actuel.liste_troupes[i].recuperation_move()

    def endommager_bat(self, bat, unit) :
        bat.endommager()
        unit.move -= 1
        self.fenetre.menu.frame_action.annuler()
        self.fenetre.menu.frame_action.action_case()

    def vendre_ressources(self, num_joueur, qte:tuple) : # Fonction qui permet de vendre des ressources.
        # num_joueur-1 permet de recuperer le bon joueur dans la liste de joueur.
        self.joueurs[num_joueur-1].diminuer_ressources(qte)
        self.marche.vendre_bois(qte[0])
        self.marche.vendre_pierre(qte[1])
        self.marche.vendre_fer(qte[2])
        self.marche.vendre_mana(qte[3])

    def acheter_ressources(self, num_joueur, qte:tuple) : # Fonction qui permet d'acheter des ressources.
        self.joueurs[num_joueur-1].augmenter_ressources(qte)
        self.marche.acheter_bois(qte[0])
        self.marche.acheter_pierre(qte[1])
        self.marche.acheter_fer(qte[2])
        self.marche.acheter_mana(qte[3])

    def ajouter_unite(self, num_joueur:int, unit_name:str, coords:tuple): # Fonction qui ajoute une unite au joueur.
        if self.carte.grille[coords[0]][coords[1]].unit == None and self.joueur_actuel.gold.act >= self.dico_troupe[unit_name]["cout"]: #1 Vérifier si c'est possible (argent, coordonnées),
            unit = Troupes("joueur"+str(num_joueur), unit_name, coords[0], coords[1]) #2 Créer l'unité (x = Unit(unit_name,...)),
            self.joueur_actuel.liste_troupes.append(unit) #3 L'ajouter au joueur,
            self.carte.ajouter_unite(unit, coords[0], coords[1]) #4 L'ajouter à la carte.
            self.fenetre.menu.frame_carte.charger_unite(unit)
            self.joueur_actuel.gold.act -= self.dico_troupe[unit_name]["cout"]
            self.fenetre.menu.frame_ressources.actualiser()
            unit.move = 0

            self.fenetre.menu.frame_action.annuler()
            self.fenetre.menu.frame_action.action_case()
        else :
            self.fenetre.menu.frame_action.lab_resultat.config(text = "Cette action est impossible")

    def retirer_unite(self, unit:Troupes): # Fonction qui retire une unite au joueur.
        num_joueur = int(unit.joueur[-1])
        self.joueurs[num_joueur-1].liste_troupes.remove(unit) #1 Retirer l'unité au joueur,
        self.carte.grille[unit.colonne][unit.ligne].unit = None #2 Retirer l'unité de la carte.
        self.fenetre.menu.frame_carte.mappe.delete(unit.identifiant)

    def deplacer_unite(self, unit:Troupes, depart, arrive):
        move_horizontale = depart[0] - arrive[0]
        move_verticale = depart[1] - arrive[1]
        if move_horizontale < 0 :
            for i in range(move_horizontale, 0) :
                self.carte.grille[depart[0]][depart[1]].unit.deplacer_droite() #1 Modifier les coordonnées de l'unité,
                self.carte.deplacer_unite(depart[0], depart[1], "DROITE") #2 Modifier la carte,
                self.fenetre.menu.frame_carte.deplacer_unite(unit, 1, 0) #3 Modifier l'affichage.
        elif move_horizontale > 0 :
            for i in range(0, move_horizontale) :
                self.carte.grille[depart[0]][depart[1]].unit.deplacer_gauche() #1
                self.carte.deplacer_unite(depart[0], depart[1], "GAUCHE") #2
                self.fenetre.menu.frame_carte.deplacer_unite(unit, -1, 0) #3
        if move_verticale < 0 :
            for i in range(move_verticale, 0) :
                self.carte.grille[depart[0]][depart[1]].unit.deplacer_bas() #1
                self.carte.deplacer_unite(depart[0], depart[1], "BAS") #2
                self.fenetre.menu.frame_carte.deplacer_unite(unit, 0, 1) #3
        elif move_verticale > 0 :
            for i in range(0, move_verticale) :
                self.carte.grille[depart[0]][depart[1]].unit.deplacer_haut() #1
                self.carte.deplacer_unite(depart[0], depart[1], "HAUT") #2
                self.fenetre.menu.frame_carte.deplacer_unite(unit, 0, -1) #3

    ### Gestion DES BATIMENTS ###
    def ajouter_batiment(self, num_joueur:int, bat_name:str, coords:tuple):
        if self.carte.grille[coords[0]][coords[1]].building == None : # Verifie si le batiment peut etre créer,
            if self.joueur_actuel.bois.act >= self.dico_bat[bat_name]["niveau1"]["cout"]["bois"]  and self.joueur_actuel.pierre.act >= self.dico_bat[bat_name]["niveau1"]["cout"]["pierre"]:
                if self.joueur_actuel.fer.act >= self.dico_bat[bat_name]["niveau1"]["cout"]["metal"]  and self.joueur_actuel.mana.act >= self.dico_bat[bat_name]["niveau1"]["cout"]["mana"]:
                    bat = Batiments(bat_name, "joueur" + str(num_joueur), coords[0], coords[1]) #2 Créer le batiment (x = Batiments(bat_name,...)),
                    self.joueur_actuel.liste_batiments.append(bat) #3 L'ajouter au joueur,
                    self.carte.ajouter_batiment(bat, coords[0], coords[1]) #4 L'ajouter à la carte.
                    self.joueur_actuel.bois.act -= self.dico_bat[bat_name]["niveau1"]["cout"]["bois"] # Diminue le bois du joueur,
                    self.joueur_actuel.pierre.act -= self.dico_bat[bat_name]["niveau1"]["cout"]["pierre"] # Diminue la pierre du joueur,
                    self.joueur_actuel.fer.act -= self.dico_bat[bat_name]["niveau1"]["cout"]["metal"] # Diminue le fer du joueur,
                    self.joueur_actuel.mana.act -= self.dico_bat[bat_name]["niveau1"]["cout"]["mana"] # Diminue le mana du joueur.
                    self.fenetre.menu.frame_carte.charger_batt(bat)
                    self.fenetre.menu.frame_carte.positionner_avant_batt(bat)

                    self.fenetre.menu.frame_ressources.actualiser()

                    self.fenetre.menu.frame_action.annuler()
                    self.fenetre.menu.frame_action.action_case()
                else :
                    self.fenetre.menu.frame_action.lab_resultat.config(text = "Il manque du fer ou du mana.")
            else :
                self.fenetre.menu.frame_action.lab_resultat.config(text = "Il manque du bois ou de la pierre.")
        else :
            self.fenetre.menu.frame_action.lab_resultat.config(text = "Il y a déja un batiment sur cette case.")

    def retirer_batiment(self, batiment:Batiments):
        num_joueur = int(unit.joueur[-1])
        self.joueurs[num_joueur-1].liste_batiments.remove(batiment) #1 Retirer le batiment au joueur,
        self.carte.grille[unit.colonne][unit.ligne].building = None #2 Retirer le batiment de la carte.


if __name__ == "__main__" :
    jeu = Jeu(200)