from tkinter import *
from joueur import Joueur
from jeu import *
from troupes import *

class Interface_Ressources:
    def __init__(self, master, joueur, jeu):
        self.icones = dict()
        self.icones["bois"] = PhotoImage(file="img/icon_bois.png")
        self.icones["fer"] = PhotoImage(file="img/icon_fer.png")
        self.icones["pierre"] = PhotoImage(file="img/icon_pierre.png")
        self.icones["mana"] = PhotoImage(file="img/icon_mana.png")
        self.icones["gold"] = PhotoImage(file="img/icon_gold.png")
        self.jeu = jeu
        self.joueur = joueur
        self.frame = Frame(master=master)
        self.label_bois = Label(master = self.frame, text = "", image=self.icones["bois"], compound=LEFT)
        self.label_fer = Label(master = self.frame, text = "", image=self.icones["fer"], compound=LEFT)
        self.label_pierre = Label(master = self.frame, text = "", image=self.icones["pierre"], compound=LEFT)
        self.label_mana = Label(master = self.frame, text = "", image=self.icones["mana"], compound=LEFT)
        self.label_gold = Label(master = self.frame, text = "", image=self.icones["gold"], compound=LEFT)
        self.organiser()
        self.actualiser()

    def organiser(self):
        self.label_bois.config(font=("Arial", 16))
        self.label_fer.config(font=("Arial", 16))
        self.label_pierre.config(font=("Arial", 16))
        self.label_mana.config(font=("Arial", 16))
        self.label_gold.config(font=("Arial", 16))

        self.label_bois.grid(row = 0, column = 0, sticky = EW)
        self.label_fer.grid(row = 0, column = 1, sticky = EW)
        self.label_pierre.grid(row = 0, column = 2, sticky = EW)
        self.label_mana.grid(row = 0, column = 3, sticky = EW)
        self.label_gold.grid(row = 0, column = 4, sticky = EW)

    def actualiser(self):
        Bois, Fer, Pierre, Mana, Gold = self.jeu.joueur_actuel.ressources_actuelles()
        self.label_bois.config(text = str(Bois))
        if Bois > 0:
            self.label_bois.config(fg="green")
        else:
            self.label_bois.config(fg="red")

        self.label_fer.config(text= str(Fer))
        if Fer > 0:
            self.label_fer.config(fg="green")
        else:
            self.label_fer.config(fg="red")

        self.label_pierre.config(text= str(Pierre))
        if Pierre > 0:
            self.label_pierre.config(fg="green")
        else:
            self.label_pierre.config(fg="red")

        self.label_mana.config(text= str(Mana))
        if Mana > 0:
            self.label_mana.config(fg="green")
        else:
            self.label_mana.config(fg="red")

        self.label_gold.config(text= str(Gold))
        if Gold > 0:
            self.label_gold.config(fg="green")
        else:
            self.label_gold.config(fg="red")

if __name__ == "__main__":
    bebert = Joueur(1)
    def essai():
        bebert.bois.augmenter(10)
        bebert.pierre.augmenter(15)
        barre.actualiser()

    fenetre = Tk()
    fenetre.title = ("4X")
    jeu = Jeu(50, fenetre)
    barre = Interface_Ressources(fenetre, bebert, jeu)
    barre.frame.grid(row=0, column=0, columnspan=2)

    btn_test = Button(fenetre, text="modifier valeurs", command = essai)
    btn_test.grid(row=1, column=0)

    fenetre.mainloop()