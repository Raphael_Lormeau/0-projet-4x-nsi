from tkinter import *
from tkinter.ttk import *
from jeu import *
from joueur import *
from marche import *


class Interface_info_joueur:
    def __init__(self, fenetre, game):
        """ Lance un onglet qui contient les infos du joueur. """
        self.fen = fenetre
        self.jeu = game

        self.boite_onglet = Notebook(master=self.fen, height=200, width=300)

        self.onglet_bat = Frame(self.boite_onglet)
        self.onglet_bat.pack(fill=BOTH)
        self.lab_bat = Label(master=self.onglet_bat, text="Batiments :")
        self.lab_bat.grid(row=1, column=1)
        self.lab_liste_bat = Label(master=self.onglet_bat, text="self.joueur.batiments_actuels()")
        self.lab_liste_bat.grid(row=1, column=2)

        self.onglet_arm = Frame(self.boite_onglet)
        self.onglet_arm.pack(fill=BOTH)
        self.lab_arm = Label(master=self.onglet_arm, text="Armée :")
        self.lab_arm.grid(row=1, column=1, sticky=N)
        self.lab_liste_arm = Label(master=self.onglet_arm, text="")
        self.lab_liste_arm.grid(row=1, column=2)

        self.onglet_eco = Frame(self.boite_onglet)
        self.onglet_eco.pack(fill=BOTH)
        self.lab_eco = Label(master=self.onglet_eco, text='Economie :')
        self.lab_eco.grid(row=1, column=1)
        self.lab_eco_a = Label(master=self.onglet_eco, text="eco_prix_a()")
        self.lab_eco_a.grid(row=1, column=2)
        self.lab_eco_v = Label(master=self.onglet_eco, text="eco_prix_v()")
        self.lab_eco_v.grid(row=2, column=2)

        self.boite_onglet.add(self.onglet_bat, text="Batiments")
        self.boite_onglet.add(self.onglet_arm, text="Armée")
        self.boite_onglet.add(self.onglet_eco, text="Economie")

        self.boite_onglet.grid(row=1, column=3)

        self.actualiser()


    def actualiser_liste_unites(self):
        txt = ""
        for unite in self.jeu.joueur1.liste_troupes:
            txt += unite.nom +"\n"
        self.lab_liste_arm.config(text=txt[:-1])
    def actualiser(self):
        """A FINIR"""
        self.lab_liste_bat.config(text="")
        self.actualiser_liste_unites()
        self.lab_eco_a.config()
        self.lab_eco_v.config()

if __name__ == "__main__":
    j=Jeu(50)
    j.joueur1.liste_troupes.append(Troupes("joueur1","fantassin", 2,3))
    j.joueur1.liste_troupes.append(Troupes("joueur1","archer", 2,3))
    fenetre = Tk()
    interf = Interface_info_joueur(fenetre, j)
    fenetre.mainloop()
