﻿import json

def charger_dico_troupe() :
    fichier = open(file="datas/dico_troupe.json", mode="r")
    dico_troupe = json.load(fichier)
    fichier.close()
    return dico_troupe

class Troupes() :
    def __init__(self, joueur:str, name:str, colonne, ligne) :
        self.dico_troupe = charger_dico_troupe()
        self.joueur = joueur # Le joueur qui possède cette troupe,
        self.nom = name # Nom de l'unité,
        self.cout = self.dico_troupe[self.nom]["cout"] # Cout de l'unité en or,
        self.viemax = self.dico_troupe[self.nom]["vie"] # Vie maximum de l'unité,
        self.vie = self.dico_troupe[self.nom]["vie"]# Vie actuel de l'unité,
        self.attaque = self.dico_troupe[self.nom]["att"] # Attaque de l'unité,
        self.defense = self.dico_troupe[self.nom]["de"]# Defense de l'unité,
        self.move_max = self.dico_troupe[self.nom]["mov"] # Nombre d'action maximum sur la carte de l'unité,
        self.move = self.dico_troupe[self.nom]["mov"]# Nombre d'action restantes sur la carte de l'unité,
        self.skills = self.dico_troupe[self.nom]["skill"]# Compétences de l'unité (action unique a la troupe),
        self.portee = self.dico_troupe[self.nom]["portee"]# Portée pour attaquer de l'unité,
        self.colonne = colonne # coordonnées x (la colonne) de la troupe,
        self.ligne = ligne # Coordonnées y (la ligne) de la troupe.

    def __str__(self) :
        txt = ""
        txt += self.nom + " : \n"
        txt = txt + "Cout : " + str(self.cout) + "\n"
        txt = txt + "Vie : " + str(self.vie) + "/" + str(self.viemax) + "\n"
        txt = txt + "Attaque : " + str(self.attaque) + ", " + "Defense : " + str(self.defense) + "\n"
        txt = txt + "Déplacement : " + str(self.move) + "/" + str(self.move_max) + "\n"
        txt = txt + "Portée : " + str(self.portee) + "\n"
        txt = txt + "Coordonnées : " + "(" + str(self.colonne)+ "," + str(self.ligne) + ")"
        return txt

    def __repr__(self) :
        return(str(self))

    def action(self) : # Fonction qui permet d'obtenir les actions faisable par la troupe.
        action = ["deplacer_haut", "deplacer_bas", "deplacer_droite", "deplacer_gauche", "attaquer", "saboter"]
        if "construire" in self.skills : # Permet de rajouter l'action de constuire.
            action.append("construire")
        elif "echange" in self.skills : # Permet de rajouter l'action d'échanger.
            action.append("echange")
        elif "voler" in self.skills : # Permet de rajouter l'action de voler.
            action.append("voler")
        return action

    def deplacer_haut(self) : # Déplace la troupe vers le haut.
        # Retirer troupe de l'ancienne case.
        self.ligne -= 1
        # Ajouter la troupe dans la case du haut.
        self.move -= 1

    def deplacer_bas(self) : # Déplace la troupe vers le bas.
        # Retirer troupe de l'ancienne case.
        self.ligne += 1
        # Ajouter la troupe dans la case du bas.
        self.move -= 1

    def deplacer_gauche(self) : # Déplace la troupe vers la gauche.
        # Retirer troupe de l'ancienne case.
        self.colonne -= 1
        # Ajouter la troupe dans la case de gauche.
        self.move -= 1

    def deplacer_droite(self) : # Déplace la troupe vers la droite.
        # Retirer troupe de l'ancienne case.
        self.colonne += 1
        # Ajouter la troupe dans la case de droite.
        self.move -= 1

    def attaquer(self, cible) : # Fonction permettant d'attaquer.
        # (Plus la cible est loin moins l'attaque fait mal.)
        degats = self.attaque - cible.defense//2
        if degats <= 0 :
            degats = 1
        cible.vie = cible.vie - degats
        if self.nom != "Archer" or cible.nom == "Archer" : # Car les archers attaque a distance
            contre = cible.attaque - self.defense
            if contre <= 0 :
                contre = 1
            self.vie = self.vie - contre
        self.move -= 1

    def saboter(self, cible) : # Attaque ignorant la defense mais avec des degats réduits.
        if self.attaque//5 <= 0 :
            cible.vie = cible.vie - 1
        else :
            cible.vie = cible.vie - self.attaque//5
        self.move -= 1


    def choix_construction(self) : # Permet de choisir un batiment a construire si il n'en exite pas deja.
        # Choisi une construction qui démarre sur la case si il n'y a pas de batiment.
        self.move -= 1

    def echange(self) : # Permet d'echanger si le marchand est a coté d'un autre marchand ou du marché.
        # Echange des ressources ou de l'or contre d'autres ressources ou de l'or.
        self.move -= 1

    def voler(self) : # Permet de voler des ressources sur une case.
        # Vole les ressources de la case
        self.move -= 1

    def soin(self) : # Soigne un peu les troupes à chaque tour.
        if self.vie + 1 > self.viemax :
            self.vie = self.viemax
        elif self.vie < self.viemax :
            self.vie += 1

    def recuperation_move(self) :
        self.move = self.move_max

    def est_mort(self) -> bool : # Regarde si la troupe est morte
        return self.vie <= 0





if __name__ == "__main__" :
    ouvrier = Troupes("joueur1", "ouvrier", 5, 4)
    archer = Troupes("joueur1", "archer", 5, 5)
    print(ouvrier.nom, ":", ouvrier.vie, "/", ouvrier.viemax)